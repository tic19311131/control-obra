<html>
<head>
	<title>Lista de Usuarios</title>
</head>

<body>
<?php
	include("conexion.php");
	$sql="select * from usuarios";
	$resultado=mysqli_query($conexion,$sql);
?>
	<h1>Lista de Usuarios</h1>
	<a href="agregar.php">Nuevo Usuario</a><br><br>
	
	<table border="1">
		<thead>
			<tr>
				<th>Id</th>
				<th>Nombre</th>
				<th>Apellido Paterno</th>
				<th>Apellido Materno</th>
				<th>Celular</th>
				<th>Correo</th>
				<th>No. Seguro Social</th>
				<th>Tipo Usuario</th>
				<th>Contaseña</th>
				<th>Acciones</th>
			</tr>
		</thead>

		<tbody>
			<?php
				while($filas=mysqli_fetch_assoc($resultado))
				{
			?>
			<tr>
				<td> <?php echo $filas['id']  ?></td>
				<td> <?php echo $filas['nombre']  ?></td>
				<td> <?php echo $filas['apellido_paterno']  ?></td>
				<td> <?php echo $filas['apellido_materno']  ?></td>
				<td> <?php echo $filas['celular']  ?></td>
				<td> <?php echo $filas['correo']  ?></td>
				<td> <?php echo $filas['numero_seguro_social']  ?></td>
				<td> <?php echo $filas['tipo_usuario']  ?></td>
				<td> <?php echo $filas['pwd']  ?></td>
				<td> 
				<?php  echo "<a href='editar.php?id=".$filas['id']."'>EDITAR</a>"; ?>
				<?php  echo "<a href='eliminar.php?id=".$filas['id']."'>ELIMINAR</a>"; ?>
				
				</td>
			</tr>
			<?php
				}
			?>
		</tbody>
	</table>
	<?php 
		mysqli_close($conexion);
	?>	
</body>
</html>
